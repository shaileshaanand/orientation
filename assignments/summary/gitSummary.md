# Git

Git is a distributed version control system used to track changes in source code during the Software Development cycle.

Git was created by Linus Torvalds in 2005 for development of the Linux Kernel.

## Repository

A Git repository stores the entire revision history and the authors concerned of all the files of a project.

## Commit

Every logical change in git is considered a `commit`. A commit is a set of changes across multiple files and represents a logical addition/change to the project.

## Elements of Git Workflow

Git Workflow includes these areas:

 - **Working Directory** : The working tree is the area that we see and edit the project in.

 - **Staging Area** : When we want to finalize our changes after testing that it works, we add them to the the staging area. Code lives here temporarily and is ultimately to be add to the repository.
 
 - **Repository** : This is where our finalized code lives, it is tracked so we can see what changes have been made to it over time and by whom.

![Git Areas](https://miro.medium.com/max/858/1*diRLm1S5hkVoh5qeArND0Q.png)

## Some Basic Commands

 - `git init` : Initializes an empty git repository in the curernt directory.
 - `git clone` : Creates a local copy of git repository that exists remotely.
 - `git add` : Stages a change i.e. adds a change to the staging area.
 - `git commit` : Adds the set of changes currently in the staging area to the repository, i.e "commits" the changes to the repository. A commit also includes a commit message and Author's Name and Email ID.
 - `git status` : shows the status of the repository, i.e. list of new files, modified files, staged files etc.
 - `git log` : shows a list of recent commits with author and commit messages.
 - `git branch` : creates a new branch in the repository which can worked on independent of the master copy/branch.
 - `git merge` : merges a branch to current branch thus incorporating all its new changes in current branch.
 - `git pull` : brings new changes in a remote repository to the local repository.
 - `git push` : sends the changes made locally to the remote repository.
