# Docker

Docker is a software platform that simplifies the process of building, running, managing and distributing applications.

## The Problem

 > "But it works on my machine!" - *A developer*

   Docker containerizes application to include all project files, dependencies etc. to match the development and production environments.

 > "How big a server do we need?"
   
   Docker helps in scaling up the infrastructure as needed and then scaling back to save costs when not needed.
 
 > "Do we need separate Virtual Machines for each App?"

   docker helps deploy multiple apps on the same server with isolated environment and dependencies with much less overhead than a Virtual Machine.
![Docker](https://www.docker.com/sites/default/files/d8/styles/large/public/2018-11/container-what-is-container.png?itok=vle7kjDj)
## Docker Terminology

1. **Docker Container** : A container contains everything that is required to run the application including, project files, dependencies, configuration files, evnironment variables etc.

2. **Docker Image** : A docker image is a template for creating a container.

3. **Docker Hub** : Docker hub is a service that hosts docker images.

4. **Dockerfile** : Dockerfile contains a list of instructions to build a docker image.

## Basic Docker Commands
![](https://www.google.com/url?sa=i&url=https%3A%2F%2Flyndametref.github.io%2Fdocker4r-presentation%2Fdocs%2F&psig=AOvVaw19KHGu6tMHrl59de9-avYe&ust=1604077206571000&source=images&cd=vfe&ved=0CAIQjRxqFwoTCLCl5MGj2uwCFQAAAAAdAAAAABAD)
1. `docker pull` : Downloads a docker image from dockerhub.
2. `docker run` : Creates and runs a container from a docker image.
3. `docker ps` or `docker container ls` : Shows all running containers.
4. `docker stop` : stops a running container.
5. `docker rm` : docker stores every container that is created when `run` command is executed, `docker rm` deletes these containers. (use `-f` flag to delete a running container)
6. `docker build` : Builds a docker image from the `Dockerfile`
7. `docker tag` : Used to give a name to a specific docker image version/build
8. `docker push` : Pushes the specified docker image to dockerhub. 

